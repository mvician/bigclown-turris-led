#!/bin/bash

led1="/sys/class/leds/omnia-led:user1/color"
led2="/sys/class/leds/omnia-led:user2/color"

function led {
	led=$1
	echo "$led"
	color=$(cat $led)
	echo -ne "$color\t"
	if [ "$color" == "255 255 255" ]; then
		echo -n "white - "
		printf "\e[38;2;255;255;255mWHITE\x1b[0m\n"
	elif [ "$color" == "0 0 255" ]; then
		echo -n "blue - "
		printf "\e[38;2;0;0;255mBLUE\x1b[0m\n"
	elif [ "$color" == "0 255 0" ]; then
		echo -n "green - "
		printf "\e[38;2;0;255;0mGREEN\x1b[0m\n"
	elif [ "$color" == "255 0 127" ]; then
		echo -n "orange red - "
		printf "\e[38;2;255;0;127mORANGE RED\x1b[0m\n"
	elif [ "$color" == "255 255 0" ]; then
		echo -n "orange - "
		printf "\e[38;2;255;255;0mORANGE\x1b[0m\n"
	elif [ "$color" == "255 0 0" ]; then
		echo -n "red - "
		printf "\e[38;2;255;0;0mRED\x1b[0m\n"
	elif [ "$color" == "0 0 0" ]; then
		echo -n "black - "
		printf "\e[38;2;0;0;0mBLACK\x1b[0m\n"
	fi
	echo ""
}

led $led1
led $led2
