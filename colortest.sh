#!/bin/bash

led1="/sys/class/leds/omnia-led:user1/color"
led2="/sys/class/leds/omnia-led:user2/color"

val1="$(cat $led1)"
val2="$(cat $led2)"

function led {
	color=$1
	echo "$color" | tee $led1 $led2
	sleep 1
}

# black
led "0 0 0"
# white
led "255 255 255"
# blue
led "0 0 255"
# green
led "0 255 0"
# orange
led "255 255 0"
# orange red
led "255 0 127"
# red
led "255 0 0"
# white again
led "255 255 255"

echo "$val1" | tee $led1
echo "$val2" | tee $led2
