## Prepare device

	git clone https://gitlab.labs.nic.cz/mvician/bigclown-turris-led /srv/bigclown-turris-led
	opkg update
	opkg install mosquitto-client
	cd /srv/bigclown-turris-led

## Enable chosen LED in Luci

- Go to Luci
- Go to System -> Rainbow
- Click on Add for the LEDs
- Choose color whcich you want and select Status: On

## Configuration

	cp bigclown.yml.example bigclown.yml

And change the file to your purposes.

## Autostart

	ln -s /srv/bigclown-turris-led/bigclown-led.init /etc/init.d/bigclown-led.init
	/etc/init.d/bigclown-led.init start
	/etc/init.d/bigclown-led.init enable

Check that the service is running

	ps | grep turris-led
